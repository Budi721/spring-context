package lecture2.beans;

import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
public class Cat {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Cat.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .toString();
    }
}
