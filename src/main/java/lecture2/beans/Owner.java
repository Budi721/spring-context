package lecture2.beans;

import org.springframework.stereotype.Component;

@Component
public record Owner(Cat cat) {

}
