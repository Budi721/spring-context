package lecture2.main;

import lecture2.config.ProjectConfig;
import lecture2.beans.Cat;
import lecture2.beans.Owner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        try (var context = new AnnotationConfigApplicationContext(ProjectConfig.class)) {
            Cat cat = context.getBean(Cat.class);
            cat.setName("Budi");
            Owner owner = context.getBean(Owner.class);

            System.out.println(cat);
            System.out.println(owner);
        }
    }
}
