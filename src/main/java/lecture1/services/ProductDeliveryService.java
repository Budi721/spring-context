package lecture1.services;

import lecture1.repositories.ProductRepository;
import org.springframework.stereotype.Service;

@Service
public record ProductDeliveryService(ProductRepository productRepository) {

    public void addSomeProduct() {
        productRepository.add();
        productRepository.add();
        productRepository.add();
    }
}
