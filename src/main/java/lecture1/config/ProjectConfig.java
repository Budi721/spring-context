package lecture1.config;

import lecture1.beans.MyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@ComponentScan(basePackages = {
        "lecture1.beans",
        "lecture1.services",
        "lecture1.repositories",
})
public class ProjectConfig {

    @Bean
    @Primary
    public MyBean myBean1() {
        var myBean = new MyBean();
        myBean.setText("Hello");
        return myBean;
    }

    @Bean
    public MyBean myBean2() {
        var myBean = new MyBean();
        myBean.setText("World");
        return myBean;
    }

    @Bean(value = "B3")
    public MyBean myBean3() {
        var myBean = new MyBean();
        myBean.setText("Budi");
        return myBean;
    }
}
