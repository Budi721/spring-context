package lecture1.main;

import lecture1.beans.MyBean;
import lecture1.beans.MyBeanComponent;
import lecture1.config.ProjectConfig;
import lecture1.services.ProductDeliveryService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        try (var context = new AnnotationConfigApplicationContext(ProjectConfig.class)){
            MyBean bean = context.getBean(MyBean.class);
            MyBean bean2 = context.getBean("myBean2", MyBean.class);
            MyBean bean3 = context.getBean("B3", MyBean.class);

            MyBeanComponent beanComponent = context.getBean(MyBeanComponent.class);

            System.out.println(bean.getText());
            System.out.println(bean2.getText());
            System.out.println(bean3.getText());

            System.out.println(beanComponent);

            ProductDeliveryService service = context.getBean(ProductDeliveryService.class);
            service.addSomeProduct();
        }
    }
}
