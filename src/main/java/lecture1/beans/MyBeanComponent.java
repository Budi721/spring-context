package lecture1.beans;

import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class MyBeanComponent {

    @PostConstruct
    private void init() {

    }
}
